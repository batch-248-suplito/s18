/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
// 1.Create a function which will be able to add two numbers.
//invoke and pass 2 arguments to the addition function
function addTwoArguments(a, b) {
    console.log("Displayed sum of " + a + " and " + b + ":");
    console.log(a + b);
};
addTwoArguments(5, 15);

//invoke and pass 2 arguments to the subtraction function
function subtractTwoArguments(a, b) {
    console.log("Displayed difference of " + a + " and " + b + ":");
    console.log(a - b);
};
subtractTwoArguments(20, 5);

// 2. Create a function which will be able to multiply two numbers.
function multiplyTwoArguments(a, b) {
    console.log("The product of " + a + " and " + b + ":");
    return a * b;
};

//Create a function which will be able to divide two numbers.
function divideTwoArguments(a, b) {
    console.log("The quotient of " + a + " and " + b + ":");
    return a / b;
};

//Create a global variable called outside of the function called product.
let product = multiplyTwoArguments(50, 10);
console.log(product);

//Create a global variable called outside of the function called quotient.
let quotient = divideTwoArguments(50, 10);
console.log(quotient);

// 3. Create a function which will be able to get total area of a circle from a provided radius.
function circleTotalArea(radius) {
    let calculation = Math.PI * radius * radius;
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    return calculation.toFixed(2);
}
//Create a global variable called outside of the function called circleArea.
let circleArea = circleTotalArea(15);
console.log(circleArea);

// 4. Create a function which will be able to get total average of four numbers.
function totalAverage(a, b, c, d) {
    let add = a + b + c + d;
    let length = 4;
    let avg = add / length;
    console.log("The average of " + a + ", " + b + ", " + c + ", and " + d + ":")
    return avg;
}
//Create a global variable called outside of the function called averageVar.
let averageVar = totalAverage(20, 40, 60, 80);
console.log(averageVar);

// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
function checkPassingPercentage(myScore, TotalScore) {
    let percentage = (100 * myScore) / TotalScore;
    let isPassed = percentage > 75;
    console.log("Is " + myScore + "/" + TotalScore + " a passing score?");
    return isPassed;
}
//Create a global variable called outside of the function called isPassingScore.
let isPassingScore = checkPassingPercentage(38, 50);
console.log(isPassingScore);